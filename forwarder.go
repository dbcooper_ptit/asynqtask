package asynqtask

import (
	"asynqtask/internal/base"
	"asynqtask/internal/log"
	"sync"
	"time"
)

type forwarder struct {
	logger      *log.Logger
	broker      base.Broker
	done        chan struct{}
	queues      []string
	avgInterval time.Duration
}

type forwarderParams struct {
	logger   *log.Logger
	broker   base.Broker
	queues   []string
	interval time.Duration
}

func newForwarder(params forwarderParams) *forwarder {
	return &forwarder{
		logger:      params.logger,
		broker:      params.broker,
		done:        make(chan struct{}),
		queues:      params.queues,
		avgInterval: params.interval,
	}
}

func (f *forwarder) shutdown() {
	f.logger.Debugf("Forwarder shutting down...")
	f.done <- struct{}{}
}

func (f *forwarder) start(wg *sync.WaitGroup) {
	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			select {
			case <-f.done:
				f.logger.Debugf("Forwarder done")
				return
			case <-time.After(f.avgInterval):
				f.exec()
			}
		}
	}()
}
func (f *forwarder) exec() {
	if err := f.broker.ForwardIfReady(f.queues...); err != nil {
		f.logger.Errorf("Could not enqueue scheduled tasks: %v", err)
	}
}
