package asynqtask

import (
	"asynqtask/internal/log"
	"sync"
	"time"
)

type syncer struct {
	logger     *log.Logger
	requestsCh <-chan *syncRequest
	done       chan struct{}
	interval   time.Duration
}

type syncRequest struct {
	fn       func() error
	errMsg   string
	deadline time.Time
}

type syncerParams struct {
	logger     *log.Logger
	requestsCh <-chan *syncRequest
	interval   time.Duration
}

func newSyncer(params syncerParams) *syncer {
	return &syncer{
		logger:     params.logger,
		requestsCh: params.requestsCh,
		done:       make(chan struct{}),
		interval:   params.interval,
	}
}

func (s *syncer) shutdown() {
	s.logger.Debug("Syncer shutting down...")
	s.done <- struct{}{}
}

func (s *syncer) start(wg *sync.WaitGroup) {
	wg.Add(1)
	go func() {
		defer wg.Done()
		var requests []*syncRequest
		for {
			select {
			case <-s.done:
				for _, req := range requests {
					if err := req.fn(); err != nil {
						s.logger.Error(req.errMsg)

					}
				}
				s.logger.Debug("Syncer Done")
				return
			case req := <-s.requestsCh:
				requests = append(requests, req)
			case <-time.After(s.interval):
				var temp []*syncRequest
				for _, req := range requests {
					if req.deadline.Before(time.Now()) {
						continue
					}
					if err := req.fn(); err != nil {
						temp = append(temp, req)
					}
				}

			}
		}
	}()
}
