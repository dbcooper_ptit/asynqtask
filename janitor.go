package asynqtask

import (
	"asynqtask/internal/base"
	"asynqtask/internal/log"
	"sync"
	"time"
)

type janitor struct {
	logger      *log.Logger
	broker      base.Broker
	done        chan struct{}
	queues      []string
	avgInterval time.Duration
}

type janitorParams struct {
	logger   *log.Logger
	broker   base.Broker
	queues   []string
	interval time.Duration
}

func newJanitor(params janitorParams) *janitor {
	return &janitor{
		logger:      params.logger,
		broker:      params.broker,
		done:        make(chan struct{}),
		queues:      params.queues,
		avgInterval: params.interval,
	}
}

func (j *janitor) shutdown() {
	j.logger.Debug("Janitor shutting down")
	j.done <- struct{}{}
}

func (j *janitor) start(wg *sync.WaitGroup) {
	wg.Add(1)
	timer := time.NewTimer(j.avgInterval)
	go func() {
		defer wg.Done()
		for {
			select {
			case <-j.done:
				j.logger.Debug("Janitor Done")
			case <-timer.C:
				j.exec()
				timer.Reset(j.avgInterval)

			}
		}
	}()
}

func (j *janitor) exec() {
	for _, qname := range j.queues {
		if err := j.broker.DeleteExpiredCompletedTasks(qname); err != nil {
			j.logger.Errorf("Could not delete expired completed tasks from queue %q: %v",
				qname, err)
		}
	}
}
