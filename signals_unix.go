package asynqtask

import (
	"golang.org/x/sys/unix"
	"os"
	"os/signal"
)

func (srv *Server) waitForSignals() {
	srv.logger.Info("Send signal TSTP stop processing new tasks")
	srv.logger.Info("Send signal TERM or INT terminate the process")

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, unix.SIGTERM, unix.SIGINT, unix.SIGTSTP)
	for {
		sig := <-sigs
		if sig == unix.SIGTSTP {
			srv.Stop()
			continue
		}
		break
	}
}

func (s *Scheduler) waitForSignals() {
	s.logger.Info("Send signal TERM or INT to stop the scheduler")
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, unix.SIGTERM, unix.SIGINT)
	<-sigs
}
